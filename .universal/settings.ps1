﻿$Parameters = @{
	LogLevel = "Warning"
	DefaultEnvironment = "Integrated"
	SecurityEnvironment = "Integrated"
	ApiEnvironment = "Integrated"
	DisableUpdateCheck = $true
	ScriptBaseFolder = "scripts"
	AdminConsoleTitle = "zed-labs"
	NotificationLevel = "Warning"
	HideRunAs = $true
	DontLoadProfile = $true
	DarkTheme = $true
	HideEnvironment = $true
	HideRunOn = $true
	DisableFormatOnSave = $true
	Branding = @{
		LoadingPageTitle = 'zed-labs'
		PageNotFoundDescription = ' '
		AppNotRunningDescription = ' '
		PageNotFoundTitle = 'zed-labs'
		LoadingPageDescription = ' '
		NotAuthorizedDescription = ' '
	}
}
Set-PSUSetting @Parameters