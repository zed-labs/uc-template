﻿param([Parameter(Mandatory)]$Type,[Parameter(Mandatory)]$Username,[Parameter(Mandatory)][SecureString]$Password)
if (Vault-Get -Type $Type | Where-Object {$_.UserName -eq $Username}){return "Credential already exists."}
Vault-Add -Type $Type -Credential ([pscredential]::new($Username,$Password))