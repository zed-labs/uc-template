Set-Alias Compare-UcsCentralMo Compare-UcsCentralManagedObject
Set-Alias Sync-UcsCentralMo Sync-UcsCentralManagedObject
Set-Alias Get-UcsCentralMo Get-UcsCentralManagedObject
Set-Alias Remove-UcsCentralMo Remove-UcsCentralManagedObject
Set-Alias Add-UcsCentralMo Add-UcsCentralManagedObject
Set-Alias Set-UcsCentralMo Set-UcsCentralManagedObject
Set-Alias Acknowledge-UcsCentralFault Confirm-UcsCentralFault
Set-Alias Get-UcsCentralUcsDomain Get-UcsCentralExtpolClient
Set-Alias Associate-UcsCentralServiceProfile Connect-UcsCentralServiceProfile
Set-Alias Disassociate-UcsCentralServiceProfile Disconnect-UcsCentralServiceProfile
##############################################################################
#.SYNOPSIS
# Remove a Fex
#
#.DESCRIPTION
# Remove a Fex
#
##############################################################################
function FnRemoveUcsCentralFex([switch]$Xml, [switch]$Force)
{
	if($Xml.IsPresent -and $Force.IsPresent)
	{
		$input | Set-UcsCentralEquipmentFexOperation -AdminState remove -Xml -Force
	}
	elseif($Xml.IsPresent)
	{
		$input | Set-UcsCentralEquipmentFexOperation -AdminState remove -Xml
	}
	elseif($Force.IsPresent)
	{
		$input | Set-UcsCentralEquipmentFexOperation -AdminState remove -Force
	}
	else
	{
		$input | Set-UcsCentralEquipmentFexOperation -AdminState remove 
	}
}
Set-Alias Remove-UcsCentralFex FnRemoveUcsCentralFex
##############################################################################
#.SYNOPSIS
# Decommission a Fex
#
#.DESCRIPTION
# Decommission a Fex
#
##############################################################################
function FnDecommissionUcsCentralFex([switch]$Xml, [switch]$Force)
{
	if($Xml.IsPresent -and $Force.IsPresent)
	{
		$input | Set-UcsCentralEquipmentFexOperation -AdminState decommission -Xml -Force
	}
	elseif($Xml.IsPresent)
	{
		$input | Set-UcsCentralEquipmentFexOperation -AdminState decommission -Xml
	}
	elseif($Force.IsPresent)
	{
		$input | Set-UcsCentralEquipmentFexOperation -AdminState decommission -Force
	}
	else
	{
		$input | Set-UcsCentralEquipmentFexOperation -AdminState decommission 
	}
}
Set-Alias Decommission-UcsCentralFex FnDecommissionUcsCentralFex
##############################################################################
#.SYNOPSIS
# Recommission a Fex
#
#.DESCRIPTION
# Recommission a Fex
#
##############################################################################
function FnRecommissionUcsCentralFex([switch]$Xml, [switch]$Force)
{
	if($Xml.IsPresent -and $Force.IsPresent)
	{
		$input | Set-UcsCentralFabricSwChPhEpOperation -AdminState enabled -Xml -Force
	}
	elseif($Xml.IsPresent)
	{
		$input | Set-UcsCentralFabricSwChPhEpOperation -AdminState enabled -Xml
	}
	elseif($Force.IsPresent)
	{
		$input | Set-UcsCentralFabricSwChPhEpOperation -AdminState enabled -Force
	}
	else
	{
		$input | Set-UcsCentralFabricSwChPhEpOperation -AdminState enabled 
	}
}
Set-Alias Recommission-UcsCentralFex FnRecommissionUcsCentralFex
##############################################################################
#.SYNOPSIS
# Acknowledge a Fex
#
#.DESCRIPTION
# Acknowledge a Fex
#
##############################################################################
function FnAcknowledgeUcsCentralFex([switch]$Xml, [switch]$Force)
{
	if($Xml.IsPresent -and $Force.IsPresent)
	{
		$input | Set-UcsCentralEquipmentFexOperation -AdminState re-acknowledge -Xml -Force
	}
	elseif($Xml.IsPresent)
	{
		$input | Set-UcsCentralEquipmentFexOperation -AdminState re-acknowledge -Xml
	}
	elseif($Force.IsPresent)
	{
		$input | Set-UcsCentralEquipmentFexOperation -AdminState re-acknowledge -Force
	}
	else
	{
		$input | Set-UcsCentralEquipmentFexOperation -AdminState re-acknowledge 
	}
}
Set-Alias Acknowledge-UcsCentralFex FnAcknowledgeUcsCentralFex
##############################################################################
#.SYNOPSIS
# Decommission a ServerUnit
#
#.DESCRIPTION
# Decommission a ServerUnit
#
##############################################################################
function FnDecommissionUcsCentralServerUnit([switch]$Xml, [switch]$Force)
{
	if($Xml.IsPresent -and $Force.IsPresent)
	{
		$input | Set-UcsCentralComputePhysicalOperation -Lc decommission -Xml -Force
	}
	elseif($Xml.IsPresent)
	{
		$input | Set-UcsCentralComputePhysicalOperation -Lc decommission -Xml
	}
	elseif($Force.IsPresent)
	{
		$input | Set-UcsCentralComputePhysicalOperation -Lc decommission -Force
	}
	else
	{
		$input | Set-UcsCentralComputePhysicalOperation -Lc decommission 
	}
}
Set-Alias Decommission-UcsCentralServerUnit FnDecommissionUcsCentralServerUnit
##############################################################################
#.SYNOPSIS
# Recommission a ServerUnit
#
#.DESCRIPTION
# Recommission a ServerUnit
#
##############################################################################
function FnRecommissionUcsCentralServerUnit([switch]$Xml, [switch]$Force)
{
	if($Xml.IsPresent -and $Force.IsPresent)
	{
		$input | Set-UcsCentralFabricComputePhEpOperation -AdminState enabled -Xml -Force
	}
	elseif($Xml.IsPresent)
	{
		$input | Set-UcsCentralFabricComputePhEpOperation -AdminState enabled -Xml
	}
	elseif($Force.IsPresent)
	{
		$input | Set-UcsCentralFabricComputePhEpOperation -AdminState enabled -Force
	}
	else
	{
		$input | Set-UcsCentralFabricComputePhEpOperation -AdminState enabled 
	}
}
Set-Alias Recommission-UcsCentralServerUnit FnRecommissionUcsCentralServerUnit
##############################################################################
#.SYNOPSIS
# Acknowledge a ServerUnit
#
#.DESCRIPTION
# Acknowledge a ServerUnit
#
##############################################################################
function FnAcknowledgeUcsCentralServerUnit([switch]$Xml, [switch]$Force)
{
	if($Xml.IsPresent -and $Force.IsPresent)
	{
		$input | Set-UcsCentralComputePhysicalOperation -Lc rediscover -Xml -Force
	}
	elseif($Xml.IsPresent)
	{
		$input | Set-UcsCentralComputePhysicalOperation -Lc rediscover -Xml
	}
	elseif($Force.IsPresent)
	{
		$input | Set-UcsCentralComputePhysicalOperation -Lc rediscover -Force
	}
	else
	{
		$input | Set-UcsCentralComputePhysicalOperation -Lc rediscover 
	}
}
Set-Alias Acknowledge-UcsCentralServerUnit FnAcknowledgeUcsCentralServerUnit
##############################################################################
#.SYNOPSIS
# Remove a ServerUnit
#
#.DESCRIPTION
# Remove a ServerUnit
#
##############################################################################
function FnRemoveUcsCentralCartridge([switch]$Xml, [switch]$Force)
{
	if($Xml.IsPresent -and $Force.IsPresent)
	{
		$input | Set-UcsCentralComputeCartridgeOperation -Lc remove -Xml -Force
	}
	elseif($Xml.IsPresent)
	{
		$input | Set-UcsCentralComputeCartridgeOperation -Lc remove -Xml
	}
	elseif($Force.IsPresent)
	{
		$input | Set-UcsCentralComputeCartridgeOperation -Lc remove -Force
	}
	else
	{
		$input | Set-UcsCentralComputeCartridgeOperation -Lc remove 
	}
}
Set-Alias Remove-UcsCentralCartridge FnRemoveUcsCentralCartridge
##############################################################################
#.SYNOPSIS
# Decommission a Chassis
#
#.DESCRIPTION
# Decommission a Chassis
#
##############################################################################
function FnDecommissionUcsCentralChassis([switch]$Xml, [switch]$Force)
{
	if($Xml.IsPresent -and $Force.IsPresent)
	{
		$input | Set-UcsCentralEquipmentChassisOperation -AdminState decommission -Xml -Force
	}
	elseif($Xml.IsPresent)
	{
		$input | Set-UcsCentralEquipmentChassisOperation -AdminState decommission -Xml
	}
	elseif($Force.IsPresent)
	{
		$input | Set-UcsCentralEquipmentChassisOperation -AdminState decommission -Force
	}
	else
	{
		$input | Set-UcsCentralEquipmentChassisOperation -AdminState decommission 
	}
}
Set-Alias Decommission-UcsCentralChassis FnDecommissionUcsCentralChassis
##############################################################################
#.SYNOPSIS
# Recommission a Chassis
#
#.DESCRIPTION
# Recommission a Chassis
#
##############################################################################
function FnRecommissionUcsCentralChassis([switch]$Xml, [switch]$Force)
{
	if($Xml.IsPresent -and $Force.IsPresent)
	{
		$input | Set-UcsCentralFabricSwChPhEpOperation -AdminState enabled -Xml -Force
	}
	elseif($Xml.IsPresent)
	{
		$input | Set-UcsCentralFabricSwChPhEpOperation -AdminState enabled -Xml
	}
	elseif($Force.IsPresent)
	{
		$input | Set-UcsCentralFabricSwChPhEpOperation -AdminState enabled -Force
	}
	else
	{
		$input | Set-UcsCentralFabricSwChPhEpOperation -AdminState enabled 
	}
}
Set-Alias Recommission-UcsCentralChassis FnRecommissionUcsCentralChassis
##############################################################################
#.SYNOPSIS
# Acknowledge a Chassis
#
#.DESCRIPTION
# Acknowledge a Chassis
#
##############################################################################
function FnAcknowledgeUcsCentralChassis([switch]$Xml, [switch]$Force)
{
	if($Xml.IsPresent -and $Force.IsPresent)
	{
		$input | Set-UcsCentralEquipmentChassisOperation -AdminState re-acknowledge -Xml -Force
	}
	elseif($Xml.IsPresent)
	{
		$input | Set-UcsCentralEquipmentChassisOperation -AdminState re-acknowledge -Xml
	}
	elseif($Force.IsPresent)
	{
		$input | Set-UcsCentralEquipmentChassisOperation -AdminState re-acknowledge -Force
	}
	else
	{
		$input | Set-UcsCentralEquipmentChassisOperation -AdminState re-acknowledge 
	}
}
Set-Alias Acknowledge-UcsCentralChassis FnAcknowledgeUcsCentralChassis
##############################################################################
#.SYNOPSIS
# Remove a RackUnit
#
#.DESCRIPTION
# Remove a RackUnit
#
##############################################################################
function FnRemoveUcsCentralRackUnit([switch]$Xml, [switch]$Force)
{
	if($Xml.IsPresent -and $Force.IsPresent)
	{
		$input | Set-UcsCentralComputePhysicalOperation -Lc remove -Xml -Force
	}
	elseif($Xml.IsPresent)
	{
		$input | Set-UcsCentralComputePhysicalOperation -Lc remove -Xml
	}
	elseif($Force.IsPresent)
	{
		$input | Set-UcsCentralComputePhysicalOperation -Lc remove -Force
	}
	else
	{
		$input | Set-UcsCentralComputePhysicalOperation -Lc remove 
	}
}
Set-Alias Remove-UcsCentralRackUnit FnRemoveUcsCentralRackUnit
##############################################################################
#.SYNOPSIS
# Decommission a RackUnit
#
#.DESCRIPTION
# Decommission a RackUnit
#
##############################################################################
function FnDecommissionUcsCentralRackUnit([switch]$Xml, [switch]$Force)
{
	if($Xml.IsPresent -and $Force.IsPresent)
	{
		$input | Set-UcsCentralComputePhysicalOperation -Lc decommission -Xml -Force
	}
	elseif($Xml.IsPresent)
	{
		$input | Set-UcsCentralComputePhysicalOperation -Lc decommission -Xml
	}
	elseif($Force.IsPresent)
	{
		$input | Set-UcsCentralComputePhysicalOperation -Lc decommission -Force
	}
	else
	{
		$input | Set-UcsCentralComputePhysicalOperation -Lc decommission 
	}
}
Set-Alias Decommission-UcsCentralRackUnit FnDecommissionUcsCentralRackUnit
##############################################################################
#.SYNOPSIS
# Recommission a RackUnit
#
#.DESCRIPTION
# Recommission a RackUnit
#
##############################################################################
function FnRecommissionUcsCentralRackUnit([switch]$Xml, [switch]$Force)
{
	if($Xml.IsPresent -and $Force.IsPresent)
	{
		$input | Set-UcsCentralFabricComputePhEpOperation -AdminState enabled -Xml -Force
	}
	elseif($Xml.IsPresent)
	{
		$input | Set-UcsCentralFabricComputePhEpOperation -AdminState enabled -Xml
	}
	elseif($Force.IsPresent)
	{
		$input | Set-UcsCentralFabricComputePhEpOperation -AdminState enabled -Force
	}
	else
	{
		$input | Set-UcsCentralFabricComputePhEpOperation -AdminState enabled 
	}
}
Set-Alias Recommission-UcsCentralRackUnit FnRecommissionUcsCentralRackUnit
##############################################################################
#.SYNOPSIS
# Acknowledge a RackUnit
#
#.DESCRIPTION
# Acknowledge a RackUnit
#
##############################################################################
function FnAcknowledgeUcsCentralRackUnit([switch]$Xml, [switch]$Force)
{
	if($Xml.IsPresent -and $Force.IsPresent)
	{
		$input | Set-UcsCentralComputePhysicalOperation -Lc rediscover -Xml -Force
	}
	elseif($Xml.IsPresent)
	{
		$input | Set-UcsCentralComputePhysicalOperation -Lc rediscover -Xml
	}
	elseif($Force.IsPresent)
	{
		$input | Set-UcsCentralComputePhysicalOperation -Lc rediscover -Force
	}
	else
	{
		$input | Set-UcsCentralComputePhysicalOperation -Lc rediscover 
	}
}
Set-Alias Acknowledge-UcsCentralRackUnit FnAcknowledgeUcsCentralRackUnit
##############################################################################
#.SYNOPSIS
# Decommission a Blade
#
#.DESCRIPTION
# Decommission a Blade
#
##############################################################################
function FnDecommissionUcsCentralBlade([switch]$Xml, [switch]$Force)
{
	if($Xml.IsPresent -and $Force.IsPresent)
	{
		$input | Set-UcsCentralComputePhysicalOperation -Lc decommission -Xml -Force
	}
	elseif($Xml.IsPresent)
	{
		$input | Set-UcsCentralComputePhysicalOperation -Lc decommission -Xml
	}
	elseif($Force.IsPresent)
	{
		$input | Set-UcsCentralComputePhysicalOperation -Lc decommission -Force
	}
	else
	{
		$input | Set-UcsCentralComputePhysicalOperation -Lc decommission 
	}
}
Set-Alias Decommission-UcsCentralBlade FnDecommissionUcsCentralBlade
##############################################################################
#.SYNOPSIS
# Recommission a Blade
#
#.DESCRIPTION
# Recommission a Blade
#
##############################################################################
function FnRecommissionUcsCentralBlade([switch]$Xml, [switch]$Force)
{
	if($Xml.IsPresent -and $Force.IsPresent)
	{
		$input | Set-UcsCentralFabricComputePhEpOperation -AdminState enabled -Xml -Force
	}
	elseif($Xml.IsPresent)
	{
		$input | Set-UcsCentralFabricComputePhEpOperation -AdminState enabled -Xml
	}
	elseif($Force.IsPresent)
	{
		$input | Set-UcsCentralFabricComputePhEpOperation -AdminState enabled -Force
	}
	else
	{
		$input | Set-UcsCentralFabricComputePhEpOperation -AdminState enabled 
	}
}
Set-Alias Recommission-UcsCentralBlade FnRecommissionUcsCentralBlade
##############################################################################
#.SYNOPSIS
# Acknowledge a Blade
#
#.DESCRIPTION
# Acknowledge a Blade
#
##############################################################################
function FnAcknowledgeUcsCentralBlade([switch]$Xml, [switch]$Force)
{
	if($Xml.IsPresent -and $Force.IsPresent)
	{
		$input | Set-UcsCentralComputePhysicalOperation -Lc rediscover -Xml -Force
	}
	elseif($Xml.IsPresent)
	{
		$input | Set-UcsCentralComputePhysicalOperation -Lc rediscover -Xml
	}
	elseif($Force.IsPresent)
	{
		$input | Set-UcsCentralComputePhysicalOperation -Lc rediscover -Force
	}
	else
	{
		$input | Set-UcsCentralComputePhysicalOperation -Lc rediscover 
	}
}
Set-Alias Acknowledge-UcsCentralBlade FnAcknowledgeUcsCentralBlade
##############################################################################
#.SYNOPSIS
# Remove a Blade
#
#.DESCRIPTION
# Remove a Blade
#
##############################################################################
function FnRemoveUcsCentralBlade([switch]$Xml, [switch]$Force)
{
	if($Xml.IsPresent -and $Force.IsPresent)
	{
		$input | Set-UcsCentralComputePhysicalOperation -Lc remove -Xml -Force
	}
	elseif($Xml.IsPresent)
	{
		$input | Set-UcsCentralComputePhysicalOperation -Lc remove -Xml
	}
	elseif($Force.IsPresent)
	{
		$input | Set-UcsCentralComputePhysicalOperation -Lc remove -Force
	}
	else
	{
		$input | Set-UcsCentralComputePhysicalOperation -Lc remove 
	}
}
Set-Alias Remove-UcsCentralBlade FnRemoveUcsCentralBlade
##############################################################################
#.SYNOPSIS
# Acknowledge a Slot
#
#.DESCRIPTION
# Acknowledge a Slot
#
##############################################################################
function FnAcknowledgeUcsCentralSlot([switch]$Xml, [switch]$Force)
{
	if($Xml.IsPresent -and $Force.IsPresent)
	{
		$input | Set-UcsCentralFabricComputeSlotEpOperation -AdminState reacknowledge -Xml -Force
	}
	elseif($Xml.IsPresent)
	{
		$input | Set-UcsCentralFabricComputeSlotEpOperation -AdminState reacknowledge -Xml
	}
	elseif($Force.IsPresent)
	{
		$input | Set-UcsCentralFabricComputeSlotEpOperation -AdminState reacknowledge -Force
	}
	else
	{
		$input | Set-UcsCentralFabricComputeSlotEpOperation -AdminState reacknowledge 
	}
}
Set-Alias Acknowledge-UcsCentralSlot FnAcknowledgeUcsCentralSlot
Export-ModuleMember -Function * -Alias *

# SIG # Begin signature block
# MIIjPgYJKoZIhvcNAQcCoIIjLzCCIysCAQExDzANBglghkgBZQMEAgEFADB5Bgor
# BgEEAYI3AgEEoGswaTA0BgorBgEEAYI3AgEeMCYCAwEAAAQQH8w7YFlLCE63JNLG
# KX7zUQIBAAIBAAIBAAIBAAIBADAxMA0GCWCGSAFlAwQCAQUABCA563WDp5Gy8Uk/
# cUvu2O1nFLkcpTKGh2bPoMi+6YW0kaCCDIYwggXCMIIEqqADAgECAhAGs5ehxvr3
# 3+zjzsf3AEZXMA0GCSqGSIb3DQEBCwUAMGwxCzAJBgNVBAYTAlVTMRUwEwYDVQQK
# EwxEaWdpQ2VydCBJbmMxGTAXBgNVBAsTEHd3dy5kaWdpY2VydC5jb20xKzApBgNV
# BAMTIkRpZ2lDZXJ0IEVWIENvZGUgU2lnbmluZyBDQSAoU0hBMikwHhcNMjAxMDIx
# MDAwMDAwWhcNMjMxMDI1MjM1OTU5WjCB1TETMBEGCysGAQQBgjc8AgEDEwJVUzEb
# MBkGCysGAQQBgjc8AgECEwpDYWxpZm9ybmlhMR0wGwYDVQQPDBRQcml2YXRlIE9y
# Z2FuaXphdGlvbjERMA8GA1UEBRMIQzExODM0NzcxCzAJBgNVBAYTAlVTMRMwEQYD
# VQQIEwpDYWxpZm9ybmlhMREwDwYDVQQHEwhTYW4gSm9zZTEcMBoGA1UEChMTQ0lT
# Q08gU1lTVEVNUywgSU5DLjEcMBoGA1UEAxMTQ0lTQ08gU1lTVEVNUywgSU5DLjCC
# ASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAM4a4NqYk3Y87GtsF/vMeyKX
# mI/feQ6EfLH5mxedhBTki+8Lviz9zbN16EJfk0cT1rWI+bBHo2p49bhRMkdL3MkP
# fJQH4ts4OZDrCHDtlQ60RIxxK43/C/ZapYuMO8KhNXE613vXOBHBxqK5IGnH+LIV
# thl6bqM+FidqmIo/tj8Nijx52Ddu2RvA0cT/eS2hDZ/HZvXBddg26zjyySJr4NqZ
# 4rRJkUqh5jSEGUsl58vCK+kE9iWzeP8W1oJZE29EmOVOG309gltGaN1mN+v2kkKk
# sFa56p3YPDlYcbWRqRGsyffZNit5qh55ZJY3EFSndrVGdPR6CaZwMJXRnT8Zfg0C
# AwEAAaOCAfQwggHwMB8GA1UdIwQYMBaAFI/ofvBtMmoABSPHcJdqOpD/a+rUMB0G
# A1UdDgQWBBRAgSdqGJHDn1gp1w9Evrwh0VEUJjAxBgNVHREEKjAooCYGCCsGAQUF
# BwgDoBowGAwWVVMtQ0FMSUZPUk5JQS1DMTE4MzQ3NzAOBgNVHQ8BAf8EBAMCB4Aw
# EwYDVR0lBAwwCgYIKwYBBQUHAwMwewYDVR0fBHQwcjA3oDWgM4YxaHR0cDovL2Ny
# bDMuZGlnaWNlcnQuY29tL0VWQ29kZVNpZ25pbmdTSEEyLWcxLmNybDA3oDWgM4Yx
# aHR0cDovL2NybDQuZGlnaWNlcnQuY29tL0VWQ29kZVNpZ25pbmdTSEEyLWcxLmNy
# bDBLBgNVHSAERDBCMDcGCWCGSAGG/WwDAjAqMCgGCCsGAQUFBwIBFhxodHRwczov
# L3d3dy5kaWdpY2VydC5jb20vQ1BTMAcGBWeBDAEDMH4GCCsGAQUFBwEBBHIwcDAk
# BggrBgEFBQcwAYYYaHR0cDovL29jc3AuZGlnaWNlcnQuY29tMEgGCCsGAQUFBzAC
# hjxodHRwOi8vY2FjZXJ0cy5kaWdpY2VydC5jb20vRGlnaUNlcnRFVkNvZGVTaWdu
# aW5nQ0EtU0hBMi5jcnQwDAYDVR0TAQH/BAIwADANBgkqhkiG9w0BAQsFAAOCAQEA
# VgLs7po2IfnfWflRgmBLQMmyUJcbSDxriBNMfkV4N46mcnyE0ZUpz8NPplBtC4Je
# EnXRGpJ73xGqsAzWcBZRkK+0bQ1Lt5LCtWagcBwGbnqnw+3u1wvkXVnvGH1FvDhJ
# BLtwN28mof498HDiyBTBtVAhW6vFxz/1LiD1Ax4D+r60FX1OHKTqZBUrKHSy1tpB
# KCARqbclQmCEkYI++IbrIFOoFh22UE3giyB2rDVRZcIYnl5xQIJgSewf4+3UmdCX
# zmDFZSJo8NiBJtzeFI5giBJxg9z0TYrw0KHyj5Nn/iQI55SxNIVPhInXJQ5GW4GF
# ekulx5a1IaJyMBEKO57BojCCBrwwggWkoAMCAQICEAPxtOFfOoLxFJZ4s9fYR1ww
# DQYJKoZIhvcNAQELBQAwbDELMAkGA1UEBhMCVVMxFTATBgNVBAoTDERpZ2lDZXJ0
# IEluYzEZMBcGA1UECxMQd3d3LmRpZ2ljZXJ0LmNvbTErMCkGA1UEAxMiRGlnaUNl
# cnQgSGlnaCBBc3N1cmFuY2UgRVYgUm9vdCBDQTAeFw0xMjA0MTgxMjAwMDBaFw0y
# NzA0MTgxMjAwMDBaMGwxCzAJBgNVBAYTAlVTMRUwEwYDVQQKEwxEaWdpQ2VydCBJ
# bmMxGTAXBgNVBAsTEHd3dy5kaWdpY2VydC5jb20xKzApBgNVBAMTIkRpZ2lDZXJ0
# IEVWIENvZGUgU2lnbmluZyBDQSAoU0hBMikwggEiMA0GCSqGSIb3DQEBAQUAA4IB
# DwAwggEKAoIBAQCnU/oPsrUT8WTPhID8roA10bbXx6MsrBosrPGErDo1EjqSkbpX
# 5MTJ8y+oSDy31m7clyK6UXlhr0MvDbebtEkxrkRYPqShlqeHTyN+w2xlJJBVPqHK
# I3zFQunEemJFm33eY3TLnmMl+ISamq1FT659H8gTy3WbyeHhivgLDJj0yj7QRap6
# HqVYkzY0visuKzFYZrQyEJ+d8FKh7+g+03byQFrc+mo9G0utdrCMXO42uoPqMKhM
# 3vELKlhBiK4AiasD0RaCICJ2615UOBJi4dJwJNvtH3DSZAmALeK2nc4f8rsh82zb
# 2LMZe4pQn+/sNgpcmrdK0wigOXn93b89OgklAgMBAAGjggNYMIIDVDASBgNVHRMB
# Af8ECDAGAQH/AgEAMA4GA1UdDwEB/wQEAwIBhjATBgNVHSUEDDAKBggrBgEFBQcD
# AzB/BggrBgEFBQcBAQRzMHEwJAYIKwYBBQUHMAGGGGh0dHA6Ly9vY3NwLmRpZ2lj
# ZXJ0LmNvbTBJBggrBgEFBQcwAoY9aHR0cDovL2NhY2VydHMuZGlnaWNlcnQuY29t
# L0RpZ2lDZXJ0SGlnaEFzc3VyYW5jZUVWUm9vdENBLmNydDCBjwYDVR0fBIGHMIGE
# MECgPqA8hjpodHRwOi8vY3JsMy5kaWdpY2VydC5jb20vRGlnaUNlcnRIaWdoQXNz
# dXJhbmNlRVZSb290Q0EuY3JsMECgPqA8hjpodHRwOi8vY3JsNC5kaWdpY2VydC5j
# b20vRGlnaUNlcnRIaWdoQXNzdXJhbmNlRVZSb290Q0EuY3JsMIIBxAYDVR0gBIIB
# uzCCAbcwggGzBglghkgBhv1sAwIwggGkMDoGCCsGAQUFBwIBFi5odHRwOi8vd3d3
# LmRpZ2ljZXJ0LmNvbS9zc2wtY3BzLXJlcG9zaXRvcnkuaHRtMIIBZAYIKwYBBQUH
# AgIwggFWHoIBUgBBAG4AeQAgAHUAcwBlACAAbwBmACAAdABoAGkAcwAgAEMAZQBy
# AHQAaQBmAGkAYwBhAHQAZQAgAGMAbwBuAHMAdABpAHQAdQB0AGUAcwAgAGEAYwBj
# AGUAcAB0AGEAbgBjAGUAIABvAGYAIAB0AGgAZQAgAEQAaQBnAGkAQwBlAHIAdAAg
# AEMAUAAvAEMAUABTACAAYQBuAGQAIAB0AGgAZQAgAFIAZQBsAHkAaQBuAGcAIABQ
# AGEAcgB0AHkAIABBAGcAcgBlAGUAbQBlAG4AdAAgAHcAaABpAGMAaAAgAGwAaQBt
# AGkAdAAgAGwAaQBhAGIAaQBsAGkAdAB5ACAAYQBuAGQAIABhAHIAZQAgAGkAbgBj
# AG8AcgBwAG8AcgBhAHQAZQBkACAAaABlAHIAZQBpAG4AIABiAHkAIAByAGUAZgBl
# AHIAZQBuAGMAZQAuMB0GA1UdDgQWBBSP6H7wbTJqAAUjx3CXajqQ/2vq1DAfBgNV
# HSMEGDAWgBSxPsNpA/i/RwHUmCYaCALvY2QrwzANBgkqhkiG9w0BAQsFAAOCAQEA
# GTNKDIEzN9utNsnkyTq7tRsueqLi9ENCF56/TqFN4bHb6YHdnwHy5IjV6f4J/SHB
# 7F2A0vDWwUPC/ncr2/nXkTPObNWyGTvmLtbJk0+IQI7N4fV+8Q/GWVZy6OtqQb0c
# 1UbVfEnKZjgVwb/gkXB3h9zJjTHJDCmiM+2N4ofNiY0/G//V4BqXi3zabfuoxrI6
# Zmt7AbPN2KY07BIBq5VYpcRTV6hg5ucCEqC5I2SiTbt8gSVkIb7P7kIYQ5e7pTcG
# r03/JqVNYUvsRkG4Zc64eZ4IlguBjIo7j8eZjKMqbphtXmHGlreKuWEtk7jrDgRD
# 1/X+pvBi1JlqpcHB8GSUgDGCFg4wghYKAgEBMIGAMGwxCzAJBgNVBAYTAlVTMRUw
# EwYDVQQKEwxEaWdpQ2VydCBJbmMxGTAXBgNVBAsTEHd3dy5kaWdpY2VydC5jb20x
# KzApBgNVBAMTIkRpZ2lDZXJ0IEVWIENvZGUgU2lnbmluZyBDQSAoU0hBMikCEAaz
# l6HG+vff7OPOx/cARlcwDQYJYIZIAWUDBAIBBQCgggEVMBkGCSqGSIb3DQEJAzEM
# BgorBgEEAYI3AgEEMBwGCisGAQQBgjcCAQsxDjAMBgorBgEEAYI3AgEVMC8GCSqG
# SIb3DQEJBDEiBCAT8rjIyfSUHUKPJ+XyE8oTF9Ek+qUY3HSOVH/3eBV5ZzCBqAYK
# KwYBBAGCNwIBDDGBmTCBlqCBk4CBkABDAG8AbgBmAGkAZwB1AHIAZQAgAGEAbgBk
# ACAAZgBpAHIAbQB3AGEAcgBlACAAdQBwAGQAYQB0AGUAIAB0AGgAZQAgAE4AZQB4
# AHUAcwAgAHMAdwBpAHQAYwBoAGUAcwAgAGEAbgBkACAAVQBDAFMATQAgAGYAbwBy
# ACAAYQB6AHUAcgBlAHMAdABhAGMAazANBgkqhkiG9w0BAQEFAASCAQA7x6bO9Y+8
# WkQk6rZ/sk+0kolCkTtKmOT979fpQP21QI6tw2H2lyJ4N37DFsG1Za/JcmGMcRAx
# fAoZFZPyjd+veCWCwUn90C1boDPEST0h500eCjAKMBcXAtVFecSf8e6XrYcLoJ/k
# T0sJrJgUhWR+NJneSkkSucMe9AjALLd+csSC1nNM5Rz0os/pmZGGVAGvyx8VuHda
# FLUA73mrgF5+UlbdMdEf6KWU3yvcxEHXu9WHZpTUBw4h2XL/1Z6mQkvK9tVaq0GJ
# cVynDs141AEf3YRMt4o9MmN50KphmnoLig7XnuBtzmg1zOoh+nZ0DJtfbshthhFB
# MN06ys1GBARyoYITRTCCE0EGCisGAQQBgjcDAwExghMxMIITLQYJKoZIhvcNAQcC
# oIITHjCCExoCAQMxDzANBglghkgBZQMEAgEFADB5BgsqhkiG9w0BCRABBKBqBGgw
# ZgIBAQYLYIZIAYb5LwAGDQMwMTANBglghkgBZQMEAgEFAAQgUZ6aXN9e8GsbDj56
# Lwf6LoyQ66EeCTl1npkhypShhoMCEEABiOaX4nsMtnKqXxPGhCAYDzIwMjMwNjIz
# MDQ1MTMzWqCCD2QwggekMIIFjKADAgECAhBAAYD9DvXolVgjPR54g1q+MA0GCSqG
# SIb3DQEBCwUAMEUxCzAJBgNVBAYTAlVTMRIwEAYDVQQKEwlJZGVuVHJ1c3QxIjAg
# BgNVBAMTGVRydXN0SUQgVGltZXN0YW1waW5nIENBIDMwHhcNMjIwNTI1MjExMDU3
# WhcNMjMwODI0MjExMDU3WjBJMQswCQYDVQQGEwJVUzESMBAGA1UEChMJSWRlblRy
# dXN0MSYwJAYDVQQDEx1UcnVzdElEIFRpbWVzdGFtcCBBdXRob3JpdHkgMzCCAiIw
# DQYJKoZIhvcNAQEBBQADggIPADCCAgoCggIBALp7eYoJ+vne9oA1jzw1XoBTTOWq
# vgf2x/gV7sOXhAsCOe8Wy2Rul2R3PT+pGzyDvR/8DWoikVkAYBm0/bnIL6Y/IpOI
# f4fwVgdHHNJqIvYvbV1+PESwLzQ5A5soFLXw9E/otcCCwMaQhTOimABeo7P3lpaL
# rVX9FDLqxYHJ/YYKYAU2JRJG0z5SB2YYwRL5MhRBFTaGM9m9CdU2EK/50YHeYmoT
# kikdcDlBNKJ/qlM1CkNW3Uun2+mBPBtNkqbGcwoX41kmb+Vo6TRiq8zo7hosFlx3
# d7/0gfYmyrNGIjdH/2FrHPKUKbuIYR8WZP6C+kypSNsELZPfFGrBwjnr7jmBIwgi
# AXd52SlTXBpv6+upRRRggeLyNP+GQlhcSntTm1ByRSGIhGiWNEijNM/gYaw1JYxI
# fKgMReuTAdV99augSBY3b91uSVIhZp7hTl2ZHARK+/0IssO1YZ8ivZH3F8s52wAw
# faVA5Np23m+ZqaoC+1IrPCuYX6ZGewltvTq4hcs21IMIMcf84X1lfKqXsK+16uKk
# e2J1YCQlfp+BCONpDYtfdzfUySAFJKfs6SKflb8pgJiIPrHzVPpuSEO2/XJiMrMc
# fInp59bjzhvEayTu+PTTV7PD3FvEEnROnapip3brHmFRTlwBVo8mhl7dEXXoYI3V
# AsDRhDQJdB4xh9sbAgMBAAGjggKKMIIChjAMBgNVHRMBAf8EAjAAMA4GA1UdDwEB
# /wQEAwIHgDAWBgNVHSUBAf8EDDAKBggrBgEFBQcDCDCBhQYIKwYBBQUHAQEEeTB3
# MDAGCCsGAQUFBzABhiRodHRwOi8vY29tbWVyY2lhbC5vY3NwLmlkZW50cnVzdC5j
# b20wQwYIKwYBBQUHMAKGN2h0dHA6Ly92YWxpZGF0aW9uLmlkZW50cnVzdC5jb20v
# Y2VydHMvdGltZXN0YW1waW5nMy5wN2MwHwYDVR0jBBgwFoAUyjLwNnzHKtqRtXyH
# ihG9uCJsvwkwggE8BgNVHSAEggEzMIIBLzAIBgZngQwBBAIwDQYLYIZIAYb5LwAG
# DQEwggESBgtghkgBhvkvAAYNAzCCAQEwSgYIKwYBBQUHAgEWPmh0dHBzOi8vc2Vj
# dXJlLmlkZW50cnVzdC5jb20vY2VydGlmaWNhdGVzL3BvbGljeS90cy9pbmRleC5o
# dG1sMIGyBggrBgEFBQcCAjCBpQyBolRoaXMgY2VydGlmaWNhdGUgaGFzIGJlZW4g
# aXNzdWVkIGluIGFjY29yZGFuY2Ugd2l0aCBJZGVuVHJ1c3QncyBUcnVzdElEIENl
# cnRpZmljYXRlIFBvbGljeSBmb3VuZCBhdCBodHRwczovL3NlY3VyZS5pZGVudHJ1
# c3QuY29tL2NlcnRpZmljYXRlcy9wb2xpY3kvdHMvaW5kZXguaHRtbDBGBgNVHR8E
# PzA9MDugOaA3hjVodHRwOi8vdmFsaWRhdGlvbi5pZGVudHJ1c3QuY29tL2NybC90
# aW1lc3RhbXBpbmczLmNybDAdBgNVHQ4EFgQUrxUhDN/aCm91R5wS0B3oe7g6aTMw
# DQYJKoZIhvcNAQELBQADggIBAAp1wnClUmkKnzDA17s0y2fxHRmg+9x2N0T11Khs
# fkXSJ1iGMt2yE9jDcXJ97dQPoXYy6UDKM7S9LgAOalo6uOMt/o5+cSb7BCbv1AIe
# lIkufAy2kkGMgX9jDR99ixl6wzpkrBo5pU/y1AOs4I1VEjQK3e+tLABo0qpZjqFa
# eGXPFc/zPVPG4whcNhmh3SqImJhCIPd8JMD3hy28yOsjaHVd5GKMNFD7OMWzuSxs
# 4x1Ezk3EeeX4MYUK3YWXowQbtTLBAkwlhIpzj8KGuY+99gsdkKKqa2aMhMrToH3K
# c4S2RueOO7NjPfYni9cfS0J1JO6MYn0KGgsap8tRsDEOfuhREmvZ037i8i98+ajD
# gMtLPksemSVDn4DseD14WfjgIRsTOphWE2hk8E6yVy/xOAETbMnANGHp2AY+YI+r
# ZgmdK7OVsWJgYcSBDhcLC+xZ7EDd8q50slMs2HUSC8RbL5ka5t4eft5CY/Vuf0wm
# AtLdVvpMQVMuZErS/rQ4QkVS3uB+NtVb0lSXbbormtPsV+dzcP1eprZ5ZjxMfrSQ
# 1wNEgx/ChHWExdlE0sdfRDJcZPbWT9Ep5RwLbFyYiHM19DCwfbxX6X6MbB3ohVBG
# /V03BPRsEjs4rLSfS0dMSYoVKv7Y57jQ3md4udgE/Urd947BBKWhY0+YspAtO7NO
# JlrRMIIHuDCCBaCgAwIBAgIQQAF/lJAVu6kSuFeWPUTs7jANBgkqhkiG9w0BAQsF
# ADBKMQswCQYDVQQGEwJVUzESMBAGA1UEChMJSWRlblRydXN0MScwJQYDVQQDEx5J
# ZGVuVHJ1c3QgQ29tbWVyY2lhbCBSb290IENBIDEwHhcNMjIwMzE2MjEwOTA1WhcN
# MzMwNjEyMjEwOTA1WjBFMQswCQYDVQQGEwJVUzESMBAGA1UEChMJSWRlblRydXN0
# MSIwIAYDVQQDExlUcnVzdElEIFRpbWVzdGFtcGluZyBDQSAzMIICIjANBgkqhkiG
# 9w0BAQEFAAOCAg8AMIICCgKCAgEAqGL0RYFG7mL0RgSXLynLNWhEVrhsKhrVL4rS
# G+NAp4v8TbAP2YXsqWB8yZgj9DQ55AECnmQ2Uo/BqQSsI/AOr9ctqZykItmca/nG
# jKezl1kZS2YoNc4Zjj+7QO9iNunclA06fBhI+iQHAam7isQLK3CwXRDLzKkMs7Ti
# sMoGQOSd0M8P6YY/QOGYv/+tCxmfvUz2GjWzQQemgiuLjvGhPwo+hrcNzays9j0G
# 7QtALkJ0KfvJS+guvCvSuEfDzt3BaPIpD2q6GYK+MUiNis3uwwngauyL4r048wdv
# USsf92Kyr6T1pAfjjPyVDNazf/w/BjzA6ewNevFVLNfE0DhQkXMmsNVGBzY5Phhl
# p5fbTwsrD19K0FPgbGO/l/Zp2dheeiCbe09bxbhdeahSBtTVPca4Vu3Ljz+PRZjF
# odq7+lziqqpqqCP/ikEnmK/QkxjCG7AkX384dxg7yb5jjtXOnP5Yv4SXuV4SNNVV
# UBJfbLXyYAf3Q0Dal85ZxNQd0QNPQIsYWv9ttTMVc6sVErdfTBPw355St6bHz91L
# UoDD0S/GjUif8LYhVlZGXlwjYmVZOb2Z7+DAamzjwVrSsrxGCJ66Coy1rKapJuHV
# sfGAW44p2ioIEZT3s6nQJkCt7te4ab1iWzaydZGAYyBao0K7kfK3vSp4AXsE5t5y
# 6dpHYo0CAwEAAaOCAp0wggKZMBIGA1UdEwEB/wQIMAYBAf8CAQAwDgYDVR0PAQH/
# BAQDAgGGMIGJBggrBgEFBQcBAQR9MHswMAYIKwYBBQUHMAGGJGh0dHA6Ly9jb21t
# ZXJjaWFsLm9jc3AuaWRlbnRydXN0LmNvbTBHBggrBgEFBQcwAoY7aHR0cDovL3Zh
# bGlkYXRpb24uaWRlbnRydXN0LmNvbS9yb290cy9jb21tZXJjaWFscm9vdGNhMS5w
# N2MwHwYDVR0jBBgwFoAU7UQZwNPwBovupHu+QucmVMiONnYwggFEBgNVHSAEggE7
# MIIBNzAIBgZngQwBBAIwDQYLYIZIAYb5LwAGDQMwggEaBgtghkgBhvkvAAYNATCC
# AQkwSgYIKwYBBQUHAgEWPmh0dHBzOi8vc2VjdXJlLmlkZW50cnVzdC5jb20vY2Vy
# dGlmaWNhdGVzL3BvbGljeS90cy9pbmRleC5odG1sMIG6BggrBgEFBQcCAjCBrQyB
# qlRoaXMgVHJ1c3RJRCBDZXJ0aWZpY2F0ZSBoYXMgYmVlbiBpc3N1ZWQgaW4gYWNj
# b3JkYW5jZSB3aXRoIElkZW5UcnVzdCdzIFRydXN0SUQgQ2VydGlmaWNhdGUgUG9s
# aWN5IGZvdW5kIGF0IGh0dHBzOi8vc2VjdXJlLmlkZW50cnVzdC5jb20vY2VydGlm
# aWNhdGVzL3BvbGljeS90cy9pbmRleC5odG1sMEoGA1UdHwRDMEEwP6A9oDuGOWh0
# dHA6Ly92YWxpZGF0aW9uLmlkZW50cnVzdC5jb20vY3JsL2NvbW1lcmNpYWxyb290
# Y2ExLmNybDAdBgNVHQ4EFgQUyjLwNnzHKtqRtXyHihG9uCJsvwkwEwYDVR0lBAww
# CgYIKwYBBQUHAwgwDQYJKoZIhvcNAQELBQADggIBACtnO4f6QB6v2yDFeld3Pa1H
# 7Bmby2tSwzQ/5dB5WXmTZHlV433s7BDkpwGzK4fGBuTcx814uPUWWSwcL+f8bpfo
# zBv6p855j/AlKul1EiPMKkMlLtwdiK6sWT2x8qaTm4fMGbcpgUbQnxOo4BnzVUmg
# s3epm9/qXf9GaWXRz4maSWl4z3apD3X/5oMrriGWIiW6ivCq8bmOBUdm0I9quhW9
# Snk2JAaqkVCjs06rqE3rRblyNdrSypGzo5eBT498aCfcvDPJX2/q2PMkLvkKoXtV
# J1g4sEwDQxm2sg8QMEd2GKo+X7TqOeF8An7KOPDq9v0xtSsF4+ufFrl43vl6v4uM
# ey68wOHv6VmaGpCtWk1e6lSq9jLQqRBBg8CMwpw6niVyvkrdh4Tvu+5HLrareZBp
# 98PJ2cQzrHk1SiPcyDxSlhbXRks/TgKXTicUm3pZsKZcTvCXHcqulN1eoQ3z9azM
# IuR7RtdECz2RJUI6Xg//6ZdMMgaDnktMALgAyo9GgGGVimx4E2/aM5r0cm+RCrk9
# 56BTqahEdiGLWjgjq8dAJe4XfLtp6EmGvsw650uBbDA0Mg9nlSCFdGTMFBKlw65o
# 2oJaenMysAvE/VC39ZIEOBfk2IOj2GTYlOgHi0iIBIZf7SqdjhTAtoW7U9TTZj8S
# HRen/EEe1WEcfTawcOhJMYIDHzCCAxsCAQEwWTBFMQswCQYDVQQGEwJVUzESMBAG
# A1UEChMJSWRlblRydXN0MSIwIAYDVQQDExlUcnVzdElEIFRpbWVzdGFtcGluZyBD
# QSAzAhBAAYD9DvXolVgjPR54g1q+MA0GCWCGSAFlAwQCAQUAoIGYMBoGCSqGSIb3
# DQEJAzENBgsqhkiG9w0BCRABBDAcBgkqhkiG9w0BCQUxDxcNMjMwNjIzMDQ1MTMz
# WjArBgsqhkiG9w0BCRACDDEcMBowGDAWBBTy71bgeNSndiGNJX4ndDOhrNiLRjAv
# BgkqhkiG9w0BCQQxIgQgXAJvvqXjNL+X5/uVpLNYkFYeWOW2P5w4EhY3kVHFumUw
# DQYJKoZIhvcNAQEBBQAEggIAsFOTgFMf/ocB2VvUlhaOX+5w1CxEXNzlRq4zbCT0
# yTKZ7Yo7dvT3hnJ3M6hW91N1EOsLfSw5OQq12OnQW8H9xr1Vl24bf7BY08qiTq20
# llzetk1m/9fR9GPRJdixQOFcYhvHEAzPqD92AmxpMzCqI0hsziRbN86hFP6ZV2f0
# M1ZMZv80kqUs2RL6Wr961K7203YRoReUAVhAeOXvnyvydHpt8HrETUAzdO9d7jVq
# 1/8WdoIXMzNL8eUHDBEaRgGgVGCTJW3jvOQfvIc0/IVJsMYP4NyCItS4NZVzpqhZ
# T6OaxvtWiYk/tPyNV1YCfHpTUsk2wYeObsRoNjphO63l5gGf/Jsz8AsX08Q0FGZg
# IxAbJO3agmKQR/MEoeyrWc7oJAIALZdodpEisf9QhPeUi+NLD6wraeBovUm6/fPI
# iC/ysMbwqIxJsXMXsL2CxvZ87QmAvjHHboBW7O1gxfydNWg/w6iA+TeK87aev+gU
# zdOHUeGsrEri/nY41bvwT/F+sVBgeVuJKBVTe1dBpILSHtOw2hWV/L6a24dFCh+W
# JV7m3kJ63cgSOEfm83kpOfKtwZvze+2tj8eQXNVXCAQ0LJgD6UFyat1dSKr5HlAa
# PTULNcc7/M1NwXjiaoUPTJuhaJgDbO2TRcSpUNqGHleWwdFzuA4trhdkEKxICjxR
# nmA=
# SIG # End signature block
