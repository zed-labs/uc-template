﻿Function vault([string]$Name){
	Get-Command -Module Vault | Where {$_.Name -like "*$Name*"}
} 
Function vault-GetSecretFiles(){
	if (IsWindows){
		Get-ChildItem "$env:LOCALAPPDATA\Microsoft\PowerShell\secretmanagement\localstore\" -Recurse
	}else{
		Get-ChildItem "$HOME/.secretmanagement/localstore/" -Recurse
	}
}
Function vault-Get([string]$Username,[string]$Type,[switch]$Clipboard){
	Get-Module Microsoft.PowerShell.SecretManagement -ListAvailable | Import-Module -Global -Force
	Get-Module Microsoft.PowerShell.SecretStore -ListAvailable | Import-Module -Global -Force
	if (! (Get-Module "Microsoft.PowerShell.SecretManagement") -Or ! (Get-Module "Microsoft.PowerShell.SecretStore")){zInstall secretstore}
	$p = ConvertTo-SecureString -String "ThisIsMyPassWord!" -AsPlainText -Force
	if (! (Get-SecretVault | Where {$_.Name -eq "ITD"})){
		Register-SecretVault -Name ITD -ModuleName Microsoft.PowerShell.SecretStore
		Set-SecretStoreConfiguration -Scope CurrentUser -Authentication None -Interaction None -Confirm:$false -Password $p
	}
	Unlock-SecretStore -Password $p
	$Data = Get-SecretInfo -Vault "ITD" | Select @{l='Username';e={$_.Metadata.Username}},@{l='Type';e={$_.Metadata.Type}},@{l='Vault';e={$_.VaultName}}
	if ($Username){$Data = $Data | Where {$_.Username -like "$Username"}}
	if ($Type){$Data = $Data | Where {$_.Type -like "$Type"}}
	
	if (@($Data).Count -ge 2){
		return $Data
	}elseif($Username -And $Type){
		$Cred = Get-SecretInfo -Vault "ITD" | Where {$_.Metadata.Type -eq $Data.Type -And $_.Metadata.Username -eq $Data.Username} | Get-Secret
		if ($Clipboard){Set-Clipboard $Cred.GetNetworkCredential().Password}
		return $Cred
	}else{
		$Data
	}
}
Function vault-Add($Type,[PSCredential]$Credential,[switch]$Force){
	Get-Module Microsoft.PowerShell.SecretManagement -ListAvailable | Import-Module -Global -Force
	Get-Module Microsoft.PowerShell.SecretStore -ListAvailable | Import-Module -Global -Force
	if (! (Get-Module "Microsoft.PowerShell.SecretManagement") -Or ! (Get-Module "Microsoft.PowerShell.SecretStore")){zInstall secretstore}
	$p = ConvertTo-SecureString -String "ThisIsMyPassWord!" -AsPlainText -Force
	if (! (Get-SecretVault | Where {$_.Name -eq "ITD"})){
		Register-SecretVault -Name ITD -ModuleName Microsoft.PowerShell.SecretStore
		Set-SecretStoreConfiguration -Scope CurrentUser -Authentication None -Interaction None -Confirm:$false -Password $p
	}
	Unlock-SecretStore -Password $p
    if ($Credential -IsNot [pscredential]){
		if ($Type -eq 'intersight'){
			$inU = read-host -Prompt "API Key ID"
			write-host "Secret Key: " -NoNewLine
			while (1){
				read-host | set r
				set s -value ($s+"`n"+$r)
			if (!$r) {break}
			}
			$s = $s.trim()
			$inPW = ConvertTo-SecureString -String $s -AsPlainText -Force
		}else{
			$inU = read-host -Prompt "Username"
			$inPW = read-host -Prompt "Password" -AsSecureString
		}
        $Credential = New-Object System.Management.Automation.PSCredential ($inU, $inPW)
    }
    if (! $Type){$Type = read-host -Prompt "    Type"}	
	if (Get-SecretInfo -Vault "ITD" | Where {$_.Name -eq "$Type\$($Credential.UserName)"}){
		if (! $Force){return "`nCredential already exists.  You can overwrite the credential with -Force`n"}
	}	
	Set-Secret -Vault "ITD" -Name "$Type\$($Credential.UserName)" -Secret $Credential -Metadata @{Username=$Credential.Username;Type=$Type}
}
Function vault-Remove([string]$Username,[string]$Type){
	Get-Module Microsoft.PowerShell.SecretManagement -ListAvailable | Import-Module -Global -Force
	Get-Module Microsoft.PowerShell.SecretStore -ListAvailable | Import-Module -Global -Force
	if (! (Get-Module "Microsoft.PowerShell.SecretManagement") -Or ! (Get-Module "Microsoft.PowerShell.SecretStore")){zInstall secretstore}
	if (! (Get-SecretVault | Where {$_.Name -eq "ITD"})){
		Register-SecretVault -Name ITD -ModuleName Microsoft.PowerShell.SecretStore
		$p = ConvertTo-SecureString -String "Temporary" -AsPlainText -Force
		Set-SecretStoreConfiguration -Scope CurrentUser -Authentication None -Interaction None -Confirm:$false -Password $p
	}
    if (! $Username){return "Syntax error.  Requires -Username"}
	$Data = vault-Get | Where {$_.Username -eq $UserName -And $_.Type -eq $Type}
	if (! $Data){return "`nCredential Name/Type not found.`n"}
	Get-SecretInfo -Vault "ITD" | Where {$_.Metadata.Username -eq $Data.Username -And $_.Metadata.Type -eq $Data.Type} | Remove-Secret
}
Function vault-Set($Type,[PSCredential]$Credential){
    if (! $Type){$Type = read-host -Prompt "    Type"}
    if ($Credential -IsNot [pscredential]){
        $inU = read-host -Prompt "Username"
		$inPW = read-host -Prompt "Password" -AsSecureString
        $Credential = New-Object System.Management.Automation.PSCredential ($inU, $inPW)
    }
	Vault-Add -Type $Type -Credential $Credential -Force
}
Function vault-Destroy([switch]$Force){
	Get-Module Microsoft.PowerShell.SecretManagement -ListAvailable | Import-Module -Global -Force
	Get-Module Microsoft.PowerShell.SecretStore -ListAvailable | Import-Module -Global -Force
	if (! (Get-Module "Microsoft.PowerShell.SecretManagement") -Or ! (Get-Module "Microsoft.PowerShell.SecretStore")){zInstall secretstore}
	if (! (Get-SecretVault | Where {$_.Name -eq "ITD"})){
		Register-SecretVault -Name ITD -ModuleName Microsoft.PowerShell.SecretStore
		$p = ConvertTo-SecureString -String "Temporary" -AsPlainText -Force
		Set-SecretStoreConfiguration -Scope CurrentUser -Authentication None -Interaction None -Confirm:$false -Password $p
	}
	if (! $Force){
		write-host "This will delete the entire vault and all credentials within it."
		Do{
			write-host "`nTo continue, type the word " -NoNewLine; write-host "continue" -ForegroundColor Yellow -NoNewLine; write-host " and press enter: " -NoNewLine
			$Response = Read-Host
		} Until ($Response -eq 'continue')
	}
	vault-GetSecretFiles | Remove-Item
	Get-SecretVault | Where {$_.Name -eq "ITD"} | Unregister-SecretVault
}
Function vault-GetLegacy([string]$Username,[string]$Type,[switch]$Clipboard){
    $zvaultfile = "$HOME/.zv"
    $PSDefaultParameterValues['*:Encoding'] = 'utf8'
    if (Test-Path $zvaultfile){$zv = get-content $zvaultfile | ConvertFrom-Json}
    if ($Username){$zv = $zv | Where {$_.Username -like "$Username"}}
    if ($Type){$zv = $zv | Where {$_.Type -like "$Type"}}
    if (! $zv){return}
    if ($zv.Count -ge 1){return $zv | Select Username,Type}
    if (! $Username){return $zv | Select Username,Type}
    $nKi = (3,3,0,4,6,8,32,58,2,1,6,5,2,6,50,05,4,4,0,7,3,8,47,98)
    $Cred = New-Object System.Management.Automation.PSCredential ($($zv.Username), $($zv.Hash | convertto-securestring -Key $nKi))
	if ($Clipboard){Set-Clipboard $Cred.GetNetworkCredential().Password}
    if ($Cred){return $cred}
}
Function vault-ConvertLegacy([string]$Username,[string]$Type,[switch]$Force){
    $NewVault = vault-get
	$OldVault = vault-GetLegacy
	if ($UserName){$OldVault = $OldVault | Where {$_.Username -eq $UserName}}
	if ($Type){$OldVault = $OldVault | Where {$_.Type -eq $Type}}
	write-host "Checking entries..."
	foreach ($entry in $OldVault){
		write-host "  $($entry.Type)\$($entry.UserName)" -NoNewLine
		if ($NewVault | Where {$_.Username -eq $entry.UserName -And $_.Type -eq $entry.Type}){
			if (! $Force){
				write-host " [already exists]" -ForegroundColor Cyan; continue
			}else{
				$c = vault-GetLegacy -Type $entry.Type -Username $entry.UserName
				vault-set -Type $entry.Type -Credential $c -Force
				write-host " [updated]" -ForegroundColor DarkGreen
			}
		}else{
			$c = vault-GetLegacy -Type $entry.Type -Username $entry.UserName
			vault-set -Type $entry.Type -Credential $c
			write-host " [added]" -ForegroundColor DarkGreen
		}
	}
}